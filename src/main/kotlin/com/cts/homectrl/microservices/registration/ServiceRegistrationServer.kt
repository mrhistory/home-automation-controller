package com.cts.homectrl.microservices.registration

import org.springframework.boot.runApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@SpringBootApplication
@EnableEurekaServer
class ServiceRegistrationServer

fun main(args: Array<String>) {
    System.setProperty("spring.config.name", "registration-server")
    runApplication<ServiceRegistrationServer>(*args)
}
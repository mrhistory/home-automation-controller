package com.cts.homectrl.microservices.lamps

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@EnableAutoConfiguration
@EnableDiscoveryClient
@SpringBootApplication
class LampsServer

fun main(args: Array<String>) {
    System.setProperty("spring.config.name", "lamps-server")
    runApplication<LampsServer>(*args)
}